# Social network
This is a graduation project developed at the end of the courses from SENLA. 
In addition to the basic requirements, the project implemented additional functionality. 
Namely, the project is deployed in docker.
PostgreSQL is used as the database.

## Dev environment
- Java 11
- Maven
- Docker
- PostgreSQL
- Liquibase

## Requirements
- [Docker](https://docs.docker.com/get-docker/)

## Run the application
1. Download project
2. Run Docker app
3. Open terminal in project root directory
4. Run this command --> mvn install
5. Run this command --> docker-compose up --build -d

## Application usage
User interface on [link](http://localhost:8080/swagger-ui/index.html).

If you want to do something in the database, the project has a built-in pgAdmin at this [link](http://localhost:5050/).

## Conclusion
