package by.senla.socialnetworkingservice.repository;

import by.senla.socialnetworkingservice.model.Page;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PageRepository extends JpaRepository<Page, Long> {
    Long deleteByIdAndOwnerId(Long id, Long ownerId);
    boolean existsById(Long id);
    Optional<Page> findPageByOwnerIdAndPersonalTrue(Long ownerId);
    Optional<Page> findPageByOwnerIdAndId(Long ownerId, Long id);
    List<Page> findPagesByPageNameLikeIgnoreCase(String pageNameLike);
}
