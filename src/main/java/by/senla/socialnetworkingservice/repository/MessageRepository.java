package by.senla.socialnetworkingservice.repository;

import by.senla.socialnetworkingservice.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MessageRepository extends JpaRepository<Message, Long> {

    Long deleteAllByAddresseeIdInAndSenderIdInAndPersonalTrue(List<Long> addresseeIds, List<Long> senderIds);
    Long deleteMessageByIdAndSenderId(Long Id, Long senderId);
    Optional<Message> findMessageByIdAndSenderId(Long id, Long senderId);
    List<Message> findAllByAddresseeIdAndPersonalFalse(Long addresseeId);
    List<Message> findAllByAddresseeIdOrSenderIdAndPersonalTrue(Long addresseeId, Long senderId);
    List<Message> findAllByAddresseeIdInAndSenderIdInAndPersonalTrue(List<Long> addresseeIds, List<Long> senderIds);

}
