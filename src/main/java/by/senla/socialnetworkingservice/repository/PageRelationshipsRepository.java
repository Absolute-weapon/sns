package by.senla.socialnetworkingservice.repository;

import by.senla.socialnetworkingservice.model.PageRelationship;
import by.senla.socialnetworkingservice.model.util.PageRelationshipId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;

public interface PageRelationshipsRepository extends JpaRepository<PageRelationship, PageRelationshipId> {

    List<PageRelationship> findPageRelationshipsByRecipientIdEquals(Long id);

    @Query(value = "SELECT F2.INITIATOR_ID FROM PAGE_RELATIONSHIPS f1 " +
            "join PAGE_RELATIONSHIPS f2 on f1.RECIPIENT_ID = f2.INITIATOR_ID " +
            "where f1.INITIATOR_ID = ?1 and f2.RECIPIENT_ID = ?1 " +
            "group by f1.INITIATOR_ID, f2.INITIATOR_ID", nativeQuery = true)

    Collection<Long> getFriendForUserById(Long id);
}
