package by.senla.socialnetworkingservice.repository;

import by.senla.socialnetworkingservice.model.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttachmentRepository extends JpaRepository<Attachment, Long> {
}
