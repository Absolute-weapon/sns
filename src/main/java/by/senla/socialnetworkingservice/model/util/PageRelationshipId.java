package by.senla.socialnetworkingservice.model.util;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@EqualsAndHashCode
@Data
@NoArgsConstructor
public class PageRelationshipId implements Serializable {

    private Long initiatorId;
    private Long recipientId;

}