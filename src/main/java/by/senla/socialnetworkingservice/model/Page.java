package by.senla.socialnetworkingservice.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Blob;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "pages")
public class Page extends AbstractEntity {

    @JoinColumn(name = "owner_id")
    private Long ownerId;

    @Column(name = "personal")
    private boolean personal;

    @Column(name = "page_name")
    private String pageName;

    @Lob
    private Blob avatar;

    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "addressee_id", referencedColumnName = "id")
    private List<Message> pageMessage;
}
