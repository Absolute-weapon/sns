package by.senla.socialnetworkingservice.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "messages")
public class Message extends AbstractEntity {

    @Column(name = "message_body")
    private String messageBody;

    @Column(name = "sender_id")
    private Long senderId;

    @Column(name = "addressee_id")
    private Long addresseeId;

    @Column(name = "date_of_publication")
    private LocalDate dateOfPublication;

    @Column(name = "personal")
    private boolean personal;

    @Column(name = "date_of_edit")
    private LocalDate dateOfEdit;

    @OneToMany
    @JoinColumn(name = "message_id", referencedColumnName = "id")
    private List<Attachment> attachments;
}
