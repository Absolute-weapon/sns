package by.senla.socialnetworkingservice.model;

import by.senla.socialnetworkingservice.model.util.PageRelationshipId;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @NoArgsConstructor
 * @AllArgsConstructor
 * @IdClass(PageRelationshipId.class)
 * PageRelationship implements Serializable
 * requires because initiatorId and recipientId its composite ID for prevent save same requests in DB
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@IdClass(PageRelationshipId.class)
@Table(name = "page_relationships")
public class PageRelationship implements Serializable {

    @Id
    @JoinColumn(name = "initiator_id", referencedColumnName = "id")
    private Long initiatorId;

    @Id
    @JoinColumn(name = "recipient_id", referencedColumnName = "id")
    private Long recipientId;

    @Override
    public String toString() {
        return "PageRelationship{" +
                "initiatorId=" + initiatorId +
                ", recipientId=" + recipientId +
                '}';
    }
}
