package by.senla.socialnetworkingservice.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "attachments")
public class Attachment extends AbstractEntity{

    @Column(name = "message_id")
    private Long messageId;

    @Lob
    private byte[] attachment;

    @Column(name = "content_type")
    private String contentType;
}
