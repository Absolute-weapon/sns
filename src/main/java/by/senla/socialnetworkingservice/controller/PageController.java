package by.senla.socialnetworkingservice.controller;

import by.senla.socialnetworkingservice.security.CustomUser;
import by.senla.socialnetworkingservice.service.PageService;
import by.senla.socialnetworkingservice.service.dto.PageDTO;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/pages")
@RequiredArgsConstructor
public class PageController {

    private final PageService pageService;

    @Operation(summary = "Create public page")
    @PostMapping
    public ResponseEntity<String> createPublicPage(@AuthenticationPrincipal CustomUser principal,
                                                   @RequestBody @Valid PageDTO pageDTO) {
        pageService.createPublicPage(pageDTO, principal.getId());
        return ResponseEntity.ok().body("page created");
    }

    @Operation(summary = "Page search")
    @GetMapping("/pageSearch")
    public ResponseEntity<List<PageDTO>> findPageByNameLike(@RequestParam String pageNameLike) {
        List<PageDTO> pageByNameLike = pageService.findPageByNameLike(pageNameLike);
        return ResponseEntity.ok().body(pageByNameLike);
    }

    @Operation(summary = "Get page by id")
    @GetMapping("/{id}")
    public ResponseEntity<PageDTO> findPageById(@PathVariable Long id) {
        PageDTO pageDTO = pageService.findPageById(id);
        return ResponseEntity.ok().body(pageDTO);
    }

    @Operation(summary = "Update page")
    @PutMapping("/{id}")
    public ResponseEntity<String> updatePage(@AuthenticationPrincipal CustomUser principal,
                                             @PathVariable Long id,
                                             @RequestBody @Valid PageDTO pageDTO){
        pageService.updatePage(pageDTO, id, principal.getId());
        return ResponseEntity.ok().body("page edited");
    }

    @Operation(summary = "Get all pages")
    @GetMapping("/")
    public ResponseEntity<List<PageDTO>> getAllPages() {
        List<PageDTO> pages = pageService.getAllPages();
        return ResponseEntity.ok().body(pages);
    }

    @Operation(summary = "Delete page")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deletePage(@AuthenticationPrincipal CustomUser principal,
                                             @PathVariable Long id){
        pageService.deletePage(id, principal.getId());
        return ResponseEntity.ok().body("page deleted");
    }
}
