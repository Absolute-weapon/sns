package by.senla.socialnetworkingservice.controller;

import by.senla.socialnetworkingservice.security.CustomUser;
import by.senla.socialnetworkingservice.service.AttachmentService;
import by.senla.socialnetworkingservice.service.dto.AttachmentDTO;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/attachments")
public class AttachmentController {

    private final AttachmentService attachmentService;

    @Operation(summary = "Add attachment to message")
    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> addAttachment(@AuthenticationPrincipal CustomUser principal,
                                           @RequestPart("file") MultipartFile file,
                                           Long messageId) {
        attachmentService.addAttachment(file, messageId, principal.getId());
        return ResponseEntity.ok().body("attachment saved");
    }

    @Operation(summary = "Get attachment by id")
    @GetMapping("/{id}")
    public ResponseEntity<?> getAttachmentById(@PathVariable Long id) {
        AttachmentDTO attachment = attachmentService.getAttachment(id);
        return ResponseEntity.ok().body(attachment);
    }

    @Secured("ADMIN_ROLE")
    @Operation(summary = "Get all attachments")
    @GetMapping("/")
    public ResponseEntity<?> getAllAttachments() {
        List<AttachmentDTO> attachments = attachmentService.getAllAttachments();
        return ResponseEntity.ok().body(attachments);
    }

    @Operation(summary = "Update attachment by id")
    @PutMapping("/{id}/update")
    public ResponseEntity<?> updateAttachment(@AuthenticationPrincipal CustomUser principal,
                                              @RequestPart("file") MultipartFile file,
                                              @PathVariable Long id,
                                              @RequestBody Long messageId) {
        attachmentService.updateAttachment(file, messageId, principal.getId(), id);
        return ResponseEntity.ok().body("attachment updated");
    }

    @Secured("ROLE_ADMIN")
    @Operation(summary = "Delete attachment by id")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteAttachment(@AuthenticationPrincipal CustomUser principal,
                                              @PathVariable Long id) {
        attachmentService.deleteAttachment(id);
        return ResponseEntity.ok().body("attachment deleted");
    }
}
