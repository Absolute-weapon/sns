package by.senla.socialnetworkingservice.controller;

import by.senla.socialnetworkingservice.security.CustomUser;
import by.senla.socialnetworkingservice.service.UserService;
import by.senla.socialnetworkingservice.service.dto.UserDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Operation(summary = "Get user by id")
    @Secured("ROLE_ADMIN")
    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> getUserById(@PathVariable Long id) {
        return ResponseEntity.ok().body(userService.getUser(id));
    }

    @Operation(summary = "Register")
    @SecurityRequirements
    @PostMapping
    public ResponseEntity<String> registerNewUser(@RequestBody
                                                  @Valid UserDTO userDTO) {
        userService.createUser(userDTO);
        return ResponseEntity.ok().body("User " + userDTO.getUsername() + " registered");
    }

    @Operation(summary = "Delete user by id")
    @Secured("ROLE_ADMIN")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteUserById(@PathVariable Long id) {
        userService.deleteUser(id);
        return ResponseEntity.ok().body("User by id = " + id + " deleted");
    }

    @Operation(summary = "Change password")
    @PutMapping("/changePassword")
    public ResponseEntity<String> changePassword(@AuthenticationPrincipal CustomUser principal,
                                                 @RequestBody String password) {
        userService.changePassword(principal.getId(), password);
        return ResponseEntity.ok().body("Password changed");
    }

    @Operation(summary = "Edit user data")
    @PutMapping
    public ResponseEntity<String> editUser(@AuthenticationPrincipal CustomUser principal,
                                           @RequestBody @Valid UserDTO userDTO) {
        userService.updateUser(principal.getId(), userDTO);
        return ResponseEntity.ok().body("Change applied");
    }

    @Operation(summary = "Search user by username")
    @Secured("ROLE_ADMIN")
    @GetMapping("/search")
    public ResponseEntity<UserDTO> search(@RequestParam String username) {
        UserDTO userDTO = userService.findByUsername(username);
        return ResponseEntity.ok().body(userDTO);
    }

    @Operation(summary = "Get all users")
    @Secured("ROLE_ADMIN")
    @GetMapping
    public ResponseEntity<List<UserDTO>> getAllUsers() {
        List<UserDTO> users = userService.getAllUsers();
        return ResponseEntity.ok().body(users);
    }
}
