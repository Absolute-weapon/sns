package by.senla.socialnetworkingservice.controller;

import by.senla.socialnetworkingservice.security.CustomUser;
import by.senla.socialnetworkingservice.service.PageRelationshipsService;
import by.senla.socialnetworkingservice.service.dto.PageDTO;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/page-relationships")
public class PageRelationshipController {

    private final PageRelationshipsService pageRelationshipsService;

    @Autowired
    public PageRelationshipController(PageRelationshipsService pageRelationshipsService) {
        this.pageRelationshipsService = pageRelationshipsService;
    }

    @Operation(summary = "Get all friends of page")
    @GetMapping("/{id}/friends")
    public ResponseEntity<List<PageDTO>> getAllFriends(@PathVariable Long id) {
        List<PageDTO> friends = pageRelationshipsService.getAllFriends(id);
        return ResponseEntity.ok().body(friends);
    }

    @Operation(summary = "Get all subscribers of page")
    @GetMapping("/{id}/subscribers")
    public ResponseEntity<List<PageDTO>> getAllSubscribers(@PathVariable Long id) {
        return ResponseEntity.ok().body(pageRelationshipsService.getAllSubscribers(id));
    }

    @Operation(summary = "Subscribe on page")
    @PostMapping("/{id}/subscribe")
    public ResponseEntity<String> sendRelationshipRequest(@AuthenticationPrincipal CustomUser principal,
                                                          @PathVariable Long id) {
        pageRelationshipsService.sendRelationshipRequest(principal.getId(), id);
        return ResponseEntity.ok().body("relationship request sent");
    }

    @Operation(summary = "Unsubscribe page")
    @DeleteMapping("/{id}/unsubscribe")
    public ResponseEntity<String> unsubscribe(@AuthenticationPrincipal CustomUser principal,
                                                          @PathVariable Long id) {
        pageRelationshipsService.unsubscribe(principal.getId(), id);
        return ResponseEntity.ok().body("relationship request deleted");
    }
}
