package by.senla.socialnetworkingservice.controller;

import by.senla.socialnetworkingservice.security.CustomUser;
import by.senla.socialnetworkingservice.service.MessageService;
import by.senla.socialnetworkingservice.service.dto.MessageDTO;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/messages")
@RequiredArgsConstructor
public class MessageController {

    private final MessageService messageService;

    @Operation(summary = "Delete conversation with page")
    @DeleteMapping("/conversation/{id}")
    public ResponseEntity<String> deleteConversationWithPage(@AuthenticationPrincipal CustomUser principal,
                                                             @PathVariable Long id) {
        messageService.deleteConversationWithPage(principal.getId(), id);
        return ResponseEntity.ok().body("conversation deleted");
    }

    @Operation(summary = "Delete message by id")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteMessageById(@AuthenticationPrincipal CustomUser principal,
                                                    @PathVariable Long id) {
        messageService.deleteMessage(id, principal.getId());
        return ResponseEntity.ok().body("message deleted");
    }

    @Operation(summary = "Get conversation with page")
    @GetMapping("/conversation/{id}")
    public ResponseEntity<List<MessageDTO>> getConversationWithPage(@AuthenticationPrincipal CustomUser principal,
                                                                    @PathVariable Long id) {
        List<MessageDTO> conversationWithPage = messageService.getConversationWithPage(principal.getId() , id);
        return ResponseEntity.ok().body(conversationWithPage);
    }

    @Operation(summary = "Send private message to personal page")
    @PostMapping
    public ResponseEntity<String> sendPrivateMessage(@AuthenticationPrincipal CustomUser principal,
                                                     @RequestBody @Valid MessageDTO messageDTO) {
        messageService.sendPrivateMessage(messageDTO, principal.getId());
        return ResponseEntity.ok().body("private message sent");
    }

    @Operation(summary = "Create public post on page")
    @PostMapping("/public")
    public ResponseEntity<String> sendPublicMessage(@AuthenticationPrincipal CustomUser principal,
                                                    @RequestBody @Valid MessageDTO messageDTO) {
        messageService.sendPublicMessage(messageDTO, principal.getId());
        return ResponseEntity.ok().body("public message sent");
    }

    @Operation(summary = "Edit message")
    @PutMapping("/edit")
    public ResponseEntity<String> updateMessage(@AuthenticationPrincipal CustomUser principal,
                                                @RequestBody @Valid MessageDTO messageDTO) {
        messageService.updateMessage(messageDTO, principal.getId());
        return ResponseEntity.ok().body("message edited");
    }

    @Operation(summary = "Get message by id")
    @GetMapping("/{id}")
    public ResponseEntity<MessageDTO> getMessageById(@AuthenticationPrincipal CustomUser principal,
                                                     @PathVariable Long id) {
        return ResponseEntity.ok().body(messageService.getMessage(id, principal.getId()));
    }

    @Operation(summary = "Get posts on page with id")
    @GetMapping("/posts/{id}")
    public ResponseEntity<List<MessageDTO>> getPublicMessages(@PathVariable Long id) {
        return ResponseEntity.ok().body(messageService.getPublicMessages(id));
    }

    @Operation(summary = "Get dialogs")
    @GetMapping("/dialogs")
    public ResponseEntity<List<MessageDTO>> getDialogs(@AuthenticationPrincipal CustomUser principal) {
        return ResponseEntity.ok().body(messageService.getListOfAllConversations(principal.getId()));
    }
}
