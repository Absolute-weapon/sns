package by.senla.socialnetworkingservice.service;

import by.senla.socialnetworkingservice.service.dto.MessageDTO;

import java.util.List;

public interface MessageService {

    void sendPrivateMessage(MessageDTO message, Long senderId);

    void sendPublicMessage(MessageDTO message, Long senderId);

    void updateMessage(MessageDTO message, Long senderId);

    void deleteMessage(Long id, Long senderId);

    void deleteConversationWithPage(Long senderId, Long addresseeId);

    MessageDTO getMessage(Long id, Long senderId);

    List<MessageDTO> getPublicMessages(Long addresseeId);

    List<MessageDTO> getListOfAllConversations(Long pageId);

    List<MessageDTO> getConversationWithPage(Long senderId, Long addresseeId);
}
