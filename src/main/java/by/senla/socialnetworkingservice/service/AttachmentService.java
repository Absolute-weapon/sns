package by.senla.socialnetworkingservice.service;

import by.senla.socialnetworkingservice.service.dto.AttachmentDTO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface AttachmentService {

    void addAttachment(MultipartFile attachmentFile, Long messageId, Long senderId);

    AttachmentDTO getAttachment(Long attachmentId);

    List<AttachmentDTO> getAllAttachments();

    void updateAttachment(MultipartFile attachmentFile, Long messageId, Long senderId, Long attachmentId);

    void deleteAttachment(Long attachmentId);
}
