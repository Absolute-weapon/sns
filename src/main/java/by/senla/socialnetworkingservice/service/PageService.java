package by.senla.socialnetworkingservice.service;

import by.senla.socialnetworkingservice.service.dto.PageDTO;

import java.util.List;

public interface PageService {

    void createPublicPage(PageDTO pageDTO, Long ownerId);

    PageDTO findPageById(Long id);

    List<PageDTO> findPageByNameLike(String pageName);

    void updatePage(PageDTO pageDTO, Long id, Long ownerId);

    void deletePage(Long id, Long ownerId);

    List<PageDTO> getAllPages();
}
