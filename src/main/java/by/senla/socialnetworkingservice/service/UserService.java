package by.senla.socialnetworkingservice.service;

import by.senla.socialnetworkingservice.service.dto.UserDTO;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    void createUser(UserDTO userDTO);

    UserDTO findByUsername(String username);

    UserDTO getUser(Long id);

    void updateUser(Long id, UserDTO userDTO);

    void deleteUser(Long id);

    List<UserDTO> getAllUsers();

    void changePassword(Long id, String password);
}
