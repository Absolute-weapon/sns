package by.senla.socialnetworkingservice.service.util;

import by.senla.socialnetworkingservice.service.exception.ServiceException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public class MultipartFileUtil {

    public static byte[] getBytes(MultipartFile attachmentFile) {
        try {
            return attachmentFile.getBytes();
        } catch (IOException e) {
            throw new ServiceException("cannot get bytes from file");
        }
    }
}
