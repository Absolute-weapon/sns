package by.senla.socialnetworkingservice.service.impl;

import by.senla.socialnetworkingservice.model.Page;
import by.senla.socialnetworkingservice.model.User;
import by.senla.socialnetworkingservice.repository.PageRepository;
import by.senla.socialnetworkingservice.repository.UserRepository;
import by.senla.socialnetworkingservice.security.CustomUser;
import by.senla.socialnetworkingservice.service.UserService;
import by.senla.socialnetworkingservice.service.dto.UserDTO;
import by.senla.socialnetworkingservice.service.exception.ServiceException;
import by.senla.socialnetworkingservice.service.util.Converter;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Transactional
@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final Converter converter;
    private final PasswordEncoder passwordEncoder;
    private final PageRepository pageRepository;

    @Override
    @Transactional
    public void createUser(UserDTO userDTO) {
        if (userRepository.findByUsername(userDTO.getUsername()).isPresent()) {
            throw new ServiceException("user with this username exist");
        }
        if (userRepository.findByEmail(userDTO.getEmail()).isPresent()) {
            throw new ServiceException("user with this email exist");
        }
        userDTO.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        User user = modelMapper.map(userDTO, User.class);
        userRepository.save(user);
        Page page = new Page();
        page.setOwnerId(user.getId());
        page.setPersonal(true);
        page.setPageName(userDTO.getFirstName() + " " + userDTO.getUsername() + " " + userDTO.getLastName());
        pageRepository.save(page);
    }

    @Override
    public UserDTO findByUsername(String username) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new ServiceException("cannot find user by username = " + username));
        return modelMapper.map(user, UserDTO.class);
    }

    @Override
    public UserDTO getUser(Long id) {
        return modelMapper.map(userRepository.findById(id)
                .orElseThrow(() -> new ServiceException("cannot find user by id = " + id)), UserDTO.class);
    }

    @Override
    @Transactional
    public void updateUser(Long id, UserDTO userDTO) {
        User user = userRepository.findById(id).orElseThrow(() -> new ServiceException("cannot find user by id = " + id));
        user.setUsername(userDTO.getUsername());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setPhone(userDTO.getPhone());
        user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        user.setEmail(userDTO.getEmail());
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public List<UserDTO> getAllUsers() {
        return converter.mapList(userRepository.findAll(), UserDTO.class);
    }

    @Override
    @Transactional
    public void changePassword(Long id, String password) {
        User user = userRepository.findById(id).orElseThrow(() -> new ServiceException("Cannot find user by id = " + id));
        user.setPassword(passwordEncoder.encode(password));
        userRepository.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new ServiceException("Cannot find user by username = " + username));
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach(role -> authorities.add(new SimpleGrantedAuthority(role)));
        return new CustomUser(user.getUsername(), user.getPassword(), authorities, user.getId());
    }
}
