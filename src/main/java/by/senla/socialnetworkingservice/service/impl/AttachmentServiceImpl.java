package by.senla.socialnetworkingservice.service.impl;

import by.senla.socialnetworkingservice.model.Attachment;
import by.senla.socialnetworkingservice.model.Message;
import by.senla.socialnetworkingservice.repository.AttachmentRepository;
import by.senla.socialnetworkingservice.repository.MessageRepository;
import by.senla.socialnetworkingservice.service.AttachmentService;
import by.senla.socialnetworkingservice.service.dto.AttachmentDTO;
import by.senla.socialnetworkingservice.service.exception.ServiceException;
import by.senla.socialnetworkingservice.service.util.Converter;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import static by.senla.socialnetworkingservice.service.util.MultipartFileUtil.getBytes;

@Service
@RequiredArgsConstructor
public class AttachmentServiceImpl implements AttachmentService {

    private final AttachmentRepository attachmentRepository;
    private final Converter converter;
    private final ModelMapper modelMapper;
    private final MessageRepository messageRepository;

    @Override
    @Transactional
    public void addAttachment(MultipartFile attachmentFile, Long messageId, Long senderId) {
        Message message = messageRepository.findMessageByIdAndSenderId(messageId, senderId)
                .orElseThrow(() -> new ServiceException("You cant add attachment to this message"));
        Attachment attachment = Attachment.builder()
                .contentType(attachmentFile.getContentType())
                .attachment(getBytes(attachmentFile))
                .messageId(messageId)
                .build();
        attachmentRepository.save(modelMapper.map(attachment, Attachment.class));
    }

    @Override
    public AttachmentDTO getAttachment(Long attachmentId) {
        Attachment attachment = attachmentRepository
                .findById(attachmentId)
                .orElseThrow(() -> new ServiceException("Not found attachment by id = " + attachmentId));
        return modelMapper.map(attachment, AttachmentDTO.class);
    }

    @Override
    public List<AttachmentDTO> getAllAttachments() {
        return converter.mapList(attachmentRepository.findAll(), AttachmentDTO.class);
    }

    @Override
    @Transactional
    public void updateAttachment(MultipartFile attachmentFile, Long messageId, Long senderId, Long attachmentId) {
        Message message = messageRepository.findMessageByIdAndSenderId(messageId, senderId)
                .orElseThrow(() -> new ServiceException("You cant add attachment to this message"));
        Attachment attachmentById = attachmentRepository
                .findById(attachmentId)
                .orElseThrow(() -> new ServiceException("Attachment by id = " + attachmentId + " not found"));
        attachmentById.setAttachment(getBytes(attachmentFile));
        attachmentRepository.save(modelMapper.map(attachmentById, Attachment.class));
    }

    @Override
    @Transactional
    public void deleteAttachment(Long attachmentId) {

        attachmentRepository.deleteById(attachmentId);
    }

}
