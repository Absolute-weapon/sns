package by.senla.socialnetworkingservice.service.impl;

import by.senla.socialnetworkingservice.model.Page;
import by.senla.socialnetworkingservice.repository.PageRepository;
import by.senla.socialnetworkingservice.service.PageService;
import by.senla.socialnetworkingservice.service.dto.PageDTO;
import by.senla.socialnetworkingservice.service.exception.ServiceException;
import by.senla.socialnetworkingservice.service.util.Converter;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PageServiceImpl implements PageService {

    private final PageRepository pageRepository;
    private final ModelMapper modelMapper;
    private final Converter converter;

    @Override
    @Transactional
    public void createPublicPage(PageDTO pageDTO, Long ownerId) {
        pageDTO.setOwnerId(ownerId);
        pageDTO.setPersonal(false);
        pageRepository.save(modelMapper.map(pageDTO, Page.class));
    }

    @Override
    public PageDTO findPageById(Long id) {
        Page page = pageRepository.findById(id)
                .orElseThrow(() -> new ServiceException("Cannot find page by id = " + id));
        return modelMapper.map(page, PageDTO.class);
    }

    @Override
    public List<PageDTO> findPageByNameLike(String pageNameLike) {
        List<Page> pages = pageRepository.findPagesByPageNameLikeIgnoreCase("%" + pageNameLike + "%");
        return converter.mapList(pages, PageDTO.class);
    }

    @Override
    @Transactional
    public void updatePage(PageDTO pageDTO, Long id, Long ownerId) {
        Page page = pageRepository.findPageByOwnerIdAndId(ownerId, id)
                .orElseThrow(() -> new ServiceException("You cant edit this page"));
        page.setPageName(pageDTO.getPageName());
        page.setAvatar(pageDTO.getAvatar());
        pageRepository.save(page);
    }

    @Override
    @Transactional
    public void deletePage(Long id, Long ownerId) {
        Long rowsDeleted = pageRepository.deleteByIdAndOwnerId(id, ownerId);
        if (rowsDeleted<1) throw new ServiceException("Nothing to delete");
    }

    @Override
    public List<PageDTO> getAllPages() {
        List<Page> pages = pageRepository.findAll();
        if (pages.isEmpty()){
            throw new ServiceException("Looks like no pages in database");
        }
        return converter.mapList(pages, PageDTO.class);
    }
}
