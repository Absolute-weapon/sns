package by.senla.socialnetworkingservice.service.impl;

import by.senla.socialnetworkingservice.model.PageRelationship;
import by.senla.socialnetworkingservice.model.util.PageRelationshipId;
import by.senla.socialnetworkingservice.repository.PageRelationshipsRepository;
import by.senla.socialnetworkingservice.repository.PageRepository;
import by.senla.socialnetworkingservice.service.PageRelationshipsService;
import by.senla.socialnetworkingservice.service.dto.PageDTO;
import by.senla.socialnetworkingservice.service.exception.ServiceException;
import by.senla.socialnetworkingservice.service.util.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PageRelationshipsServiceImp implements PageRelationshipsService {

    private final Converter converter;
    private final PageRepository pageRepository;
    private final PageRelationshipsRepository pageRelationshipRepository;

    @Autowired
    public PageRelationshipsServiceImp(Converter converter, PageRepository pageRepository,
                                       PageRelationshipsRepository pageRelationshipRepository) {
        this.converter = converter;
        this.pageRepository = pageRepository;
        this.pageRelationshipRepository = pageRelationshipRepository;
    }

    @Override
    public List<PageDTO> getAllFriends(Long id) {
        Collection<Long> friendListId = pageRelationshipRepository.getFriendForUserById(id);
        if (friendListId.isEmpty()) {
            throw new ServiceException("This page dont exist or dont have friends");
        }
        return converter.mapList(pageRepository.findAllById(friendListId), PageDTO.class);
    }

    @Override
    public List<PageDTO> getAllSubscribers(Long id) {
        List<Long> subscriberIds = pageRelationshipRepository
                .findPageRelationshipsByRecipientIdEquals(id).stream()
                .map(PageRelationship::getInitiatorId).collect(Collectors.toList());
        if (subscriberIds.isEmpty()) {
            throw new ServiceException("This page dont exist or dont have subscribers");
        }
        return converter.mapList(pageRepository.findAllById(subscriberIds), PageDTO.class);
    }

    @Override
    @Transactional
    public void sendRelationshipRequest(Long initiatorId, Long recipientId) {
        PageRelationship pageRelationship = new PageRelationship();
        Long pageInitiatorId = pageRepository.findPageByOwnerIdAndPersonalTrue(initiatorId)
                .orElseThrow(() -> new ServiceException("Cannot find personal page for user by id = " + initiatorId)).getId();
        pageRelationship.setInitiatorId(pageInitiatorId);
        if (pageRepository.existsById(recipientId)){
        pageRelationship.setRecipientId(recipientId);
        pageRelationshipRepository.save(pageRelationship);} else {
            throw new ServiceException("You cant subscribe on this page");
        }
    }

    @Override
    @Transactional
    public void unsubscribe(Long initiatorId, Long recipientId) {
        Long pageInitiatorId = pageRepository.findPageByOwnerIdAndPersonalTrue(initiatorId)
                .orElseThrow(() -> new ServiceException("Cannot find personal page for user by id = " + initiatorId)).getId();
        PageRelationshipId id = new PageRelationshipId();
        id.setInitiatorId(pageInitiatorId);
        id.setRecipientId(recipientId);
        pageRelationshipRepository.deleteById(id);
    }
}
