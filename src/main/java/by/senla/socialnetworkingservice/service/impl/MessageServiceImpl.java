package by.senla.socialnetworkingservice.service.impl;

import by.senla.socialnetworkingservice.model.Attachment;
import by.senla.socialnetworkingservice.model.Message;
import by.senla.socialnetworkingservice.repository.MessageRepository;
import by.senla.socialnetworkingservice.repository.PageRepository;
import by.senla.socialnetworkingservice.service.MessageService;
import by.senla.socialnetworkingservice.service.PageService;
import by.senla.socialnetworkingservice.service.dto.MessageDTO;
import by.senla.socialnetworkingservice.service.exception.ServiceException;
import by.senla.socialnetworkingservice.service.util.Converter;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
public class MessageServiceImpl implements MessageService {

    private final MessageRepository messageRepository;
    private final PageRepository pageRepository;
    private final ModelMapper modelMapper;
    private final Converter converter;

    @Override
    @Transactional
    public void sendPrivateMessage(MessageDTO message, Long senderId) {
        if (pageRepository.findById(message.getAddresseeId())
                .orElseThrow(() -> new ServiceException("Cannot find page by id = " + message.getAddresseeId()))
                .isPersonal()) {
            Message messageToSave = modelMapper.map(message, Message.class);
            messageToSave.setSenderId(senderId);
            messageToSave.setPersonal(true);
            messageToSave.setDateOfPublication(LocalDate.now());
            messageRepository.save(messageToSave);
        } else {
            throw new ServiceException("You cant send personal message to public pages");
        }
    }

    @Override
    @Transactional
    public void deleteConversationWithPage(Long senderId, Long addresseeId) {
        List<Long> ids = List.of(senderId, addresseeId);
        Long rowsDeleted = messageRepository.deleteAllByAddresseeIdInAndSenderIdInAndPersonalTrue(ids, ids);
        if (rowsDeleted < 1) throw new ServiceException("Nothing to delete");
    }

    @Override
    @Transactional
    public void sendPublicMessage(MessageDTO message, Long senderId) {
        if (pageRepository.existsById(message.getAddresseeId())) {
            Message messageToSave = modelMapper.map(message, Message.class);
            messageToSave.setSenderId(senderId);
            messageToSave.setPersonal(false);
            messageToSave.setDateOfPublication(LocalDate.now());
            messageRepository.save(messageToSave);
        } else {
            throw new ServiceException("You cant send message to this page");
        }
    }

    @Override
    @Transactional
    public void updateMessage(MessageDTO message, Long senderId) {
        Message messageToSave = messageRepository.findMessageByIdAndSenderId(message.getId(), senderId)
                .orElseThrow(() -> new ServiceException("You cant edit this message"));
        messageToSave.setDateOfEdit(LocalDate.now());
        messageToSave.setMessageBody(message.getMessageBody());
        if (message.getAttachments() == null) {
            messageRepository.save(messageToSave);
        } else
            messageToSave.setAttachments(converter.mapList(message.getAttachments(), Attachment.class));
        messageRepository.save(messageToSave);
    }

    @Override
    @Transactional
    public void deleteMessage(Long id, Long senderId) {
        Long rowsDeleted = messageRepository.deleteMessageByIdAndSenderId(id, senderId);
        if (rowsDeleted < 1) throw new ServiceException("Nothing to delete");
    }

    @Override
    public MessageDTO getMessage(Long id, Long senderId) {
        Message message = messageRepository.findMessageByIdAndSenderId(id, senderId)
                .orElseThrow(() -> new ServiceException("Cannot find message by id = " + id));
        return modelMapper.map(message, MessageDTO.class);
    }

    @Override
    public List<MessageDTO> getPublicMessages(Long addresseeId) {
        List<Message> messageList = messageRepository.findAllByAddresseeIdAndPersonalFalse(addresseeId);
        if (messageList.isEmpty()) throw new ServiceException("Cannot find public messages by id = " + addresseeId);
        return converter.mapList(messageList, MessageDTO.class);
    }

    @Override
    public List<MessageDTO> getListOfAllConversations(Long senderId) {
        List<Message> messageList = messageRepository.findAllByAddresseeIdOrSenderIdAndPersonalTrue(senderId, senderId);
        if (messageList.isEmpty())
            throw new ServiceException("Cannot find conversations for page by id = " + senderId);
        return converter.mapList(messageList, MessageDTO.class);
    }

    @Override
    public List<MessageDTO> getConversationWithPage(Long senderId, Long addresseeId) {
        List<Long> ids = List.of(senderId, addresseeId);
        List<Message> messageList = messageRepository.findAllByAddresseeIdInAndSenderIdInAndPersonalTrue(ids, ids);
        if (messageList.isEmpty())
            throw new ServiceException("Cannot find conversation between " + senderId + " and " + addresseeId);
        return messageList.stream()
                .map(message -> modelMapper.map(message, MessageDTO.class))
                .sorted(comparing(MessageDTO::getId))
                .collect(toList());
    }
}
