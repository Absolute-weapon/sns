package by.senla.socialnetworkingservice.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class MessageDTO {

    private Long id;

    @NotNull
    private String messageBody;

    private Long addresseeId;

    private Long senderId;

    private boolean personal;

    private LocalDate dateOfPublication;

    private LocalDate dateOfEdit;

    private List<AttachmentDTO> attachments;
}
