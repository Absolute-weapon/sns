package by.senla.socialnetworkingservice.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Blob;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class PageDTO {

    private Long id;
    @NotBlank(message = "page name cannot be blank")
    private String pageName;

    private Long ownerId;

    private boolean personal;

    private Blob avatar;

    private List<MessageDTO> pageMessage;

    public void setPageMessage(List<MessageDTO> pageMessage) {
        this.pageMessage = pageMessage.stream()
                .filter(MessageDTO -> !MessageDTO.isPersonal())
                .collect(Collectors.toList());
    }
}
