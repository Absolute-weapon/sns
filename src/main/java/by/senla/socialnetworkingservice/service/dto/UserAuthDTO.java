package by.senla.socialnetworkingservice.service.dto;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
public class UserAuthDTO implements Serializable {
    private static final long serialVersionUID = 5926468583005150707L;
    @NotNull
    private String username;
    @NotNull
    private String password;
}