package by.senla.socialnetworkingservice.service.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PageRelationshipsDTO {

    private Long initiatorId;

    private Long recipientId;
}
