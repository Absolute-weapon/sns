package by.senla.socialnetworkingservice.service.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class AttachmentDTO {
    @NotNull
    private Long id;
    @NotNull
    private Long messageId;

    private byte[] attachment;

    private String contentType;
}
