package by.senla.socialnetworkingservice.service.dto;

import com.sun.istack.NotNull;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@Schema(description = "User model")
public class UserDTO {

    @NotNull
    private Long id;

    @NotBlank(message = "username cannot be blank")
    @Size(min = 3, max = 32, message = "username cannot be shorter than 3 symbols or longer than 32")
    private String username;

    @NotBlank(message = "firstname cannot be blank")
    @Size(min = 3, max = 32, message = "firstname cannot be shorter than 3 symbols or longer than 32")
    private String firstName;

    @NotBlank(message = "lastname cannot be blank")
    @Size(min = 3, max = 32, message = "firstname cannot be shorter than 3 symbols or longer than 32")
    private String lastName;

    @Size(min = 5, max = 12, message = "phone cannot be shorter than 5 symbols or longer than 12")
    private String phone;

    @NotNull
    @Size(min = 5, message = "password cannot be shorter than 5 symbols")
    private String password;

    @Email(message = "not valid email")
    private String email;

    private List<String> roles;
}
