package by.senla.socialnetworkingservice.service;

import by.senla.socialnetworkingservice.service.dto.PageDTO;

import java.util.List;

public interface PageRelationshipsService {

    List<PageDTO> getAllFriends(Long id);

    List<PageDTO> getAllSubscribers(Long id);

    void sendRelationshipRequest(Long initiatorId, Long recipientId);

    void unsubscribe(Long initiatorId, Long recipientId);
}
