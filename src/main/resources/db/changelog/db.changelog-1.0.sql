--liquibase formatted sql

--changeset Tomasz:1
create table if not exists USERS
(
    ID BIGINT auto_increment
        primary key,
    AVATAR BLOB,
    EMAIL VARCHAR(255),
    FIRST_NAME VARCHAR(255),
    LAST_NAME VARCHAR(255),
    PASSWORD VARCHAR(255),
    PHONE VARCHAR(255),
    USERNAME VARCHAR(255)
);
--changeset Tomasz:2
create table if not exists PAGES
(
    ID BIGINT auto_increment
        primary key,
    AVATAR BLOB,
    IS_PUBLIC BOOLEAN,
    OWNER_ID BIGINT,
    constraint fk_owner_id
        foreign key (OWNER_ID) references USERS (ID)
);
--changeset Tomasz:3
create table if not exists MESSAGES
(
    ID BIGINT auto_increment
        primary key,
    DATE_OF_EDIT DATE,
    DATE_OF_PUBLICATION DATE,
    MESSAGE_BODY VARCHAR(255),
    ADDRESSEE_ID BIGINT,
    SENDER_ID BIGINT,
    constraint fk_sender_id
        foreign key (SENDER_ID) references PAGES (ID),
    constraint fk_addressee_id
        foreign key (ADDRESSEE_ID) references PAGES (ID)
);
--changeset Tomasz:4
create table if not exists ATTACHMENTS
(
    ID BIGINT auto_increment
        primary key,
    ATTACHMENT BLOB,
    MESSAGE_ID BIGINT,
    constraint fk_message_id
        foreign key (MESSAGE_ID) references MESSAGES (ID)
);
--changeset Tomasz:5
create table if not exists PAGE_RELATIONSHIPS
(
    ID BIGINT auto_increment
        primary key,
    IS_APPROVED BOOLEAN,
    INITIATOR_ID BIGINT,
    RECIPIENT_ID BIGINT,
    constraint fk_initiator_id
        foreign key (INITIATOR_ID) references PAGES (ID),
    constraint fk_recipient_id
        foreign key (RECIPIENT_ID) references PAGES (ID)
);
--changeset Tomasz:6
create table if not exists USER_ROLES
(
    USER_ID BIGINT not null,
    ROLES VARCHAR(255),
    constraint fk_user_id
        foreign key (USER_ID) references USERS (ID)
);
