package by.senla.socialnetworkingservice.service.impl;

import by.senla.socialnetworkingservice.model.Message;
import by.senla.socialnetworkingservice.model.Page;
import by.senla.socialnetworkingservice.repository.MessageRepository;
import by.senla.socialnetworkingservice.repository.PageRepository;
import by.senla.socialnetworkingservice.service.MessageService;
import by.senla.socialnetworkingservice.service.dto.AttachmentDTO;
import by.senla.socialnetworkingservice.service.dto.MessageDTO;
import by.senla.socialnetworkingservice.service.util.Converter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class MessageServiceImplTest {

    private MessageService messageService;
    private final ModelMapper modelMapper = new ModelMapper();
    private final Converter converter = new Converter(modelMapper);
    private final MessageDTO messageDTO = new MessageDTO();

    private final Long id = 1L;
    private final Message message = new Message();
    private final List<AttachmentDTO> attachmentList = new ArrayList<>();

    @Mock
    private MessageRepository messageRepository;
    @Mock
    private PageRepository pageRepository;

    @BeforeEach
    void initMessageService() {
        messageService = new MessageServiceImpl(messageRepository, pageRepository, modelMapper, converter);
    }

    @Test
    void sendPrivateMessage() {
        //Given
        Page page = new Page();
        page.setPersonal(true);
        BDDMockito.given(pageRepository.findById(id)).willReturn(java.util.Optional.of(page));
        messageDTO.setAddresseeId(id);

        messageService.sendPrivateMessage(messageDTO, id);
        Mockito.verify(messageRepository).save(Mockito.any(Message.class));
    }

    @Test
    void deleteConversationWithPage() {
        //Given
        List<Long> ids = List.of(id, 2L);
        BDDMockito.given(messageRepository.deleteAllByAddresseeIdInAndSenderIdInAndPersonalTrue(ids, ids)).willReturn(1L);

        messageService.deleteConversationWithPage(id, 2L);
        Mockito.verify(messageRepository).deleteAllByAddresseeIdInAndSenderIdInAndPersonalTrue(ids, ids);
    }

    @Test
    void sendPublicMessage() {
        //Given
        messageDTO.setAddresseeId(id);
        BDDMockito.given(pageRepository.existsById(id)).willReturn(true);

        messageService.sendPublicMessage(messageDTO, id);
        Mockito.verify(messageRepository).save(Mockito.any(Message.class));
    }

    @Test
    void updateMessage() {
        //Given
        messageDTO.setAttachments(attachmentList);
        messageDTO.setId(id);
        BDDMockito.given(messageRepository.findMessageByIdAndSenderId(id, id)).willReturn(java.util.Optional.of(message));

        messageService.updateMessage(messageDTO, id);
        Mockito.verify(messageRepository).save(Mockito.any(Message.class));
    }

    @Test
    void deleteMessage() {
        //Given
        BDDMockito.given(messageRepository.deleteMessageByIdAndSenderId(id, id)).willReturn(1L);

        messageService.deleteMessage(id, id);
        Mockito.verify(messageRepository).deleteMessageByIdAndSenderId(id, id);
    }

    @Test
    void getMessage() {
        //Given
        message.setId(id);
        BDDMockito.given(messageRepository.findMessageByIdAndSenderId(id, id)).willReturn(java.util.Optional.of(message));

        Assertions.assertEquals(message.getId(), messageService.getMessage(id, id).getId());
    }

    @Test
    void getPublicMessages() {
        //Given
        List<Message> messageList = List.of(message);
        BDDMockito.given(messageRepository.findAllByAddresseeIdAndPersonalFalse(id)).willReturn(messageList);

        List<Message> messages = converter.mapList(messageService.getPublicMessages(id), Message.class);
        Assertions.assertEquals(messageList, messages);
    }

    @Test
    void getListOfAllConversations() {
        //Given
        List<Message> messageList = List.of(message);
        BDDMockito.given(messageRepository.findAllByAddresseeIdOrSenderIdAndPersonalTrue(id, id)).willReturn(messageList);

        List<Message> messages = converter.mapList(messageService.getListOfAllConversations(id), Message.class);
        Assertions.assertEquals(messageList, messages);
    }

    @Test
    void getConversationWithPage() {
        List<Long> ids = List.of(id, 2L);
        message.setSenderId(id);
        message.setAddresseeId(2L);
        List<Message> messageList = List.of(message);
        BDDMockito.given(messageRepository.findAllByAddresseeIdInAndSenderIdInAndPersonalTrue(ids, ids)).willReturn(messageList);

        List<Message> messages = converter.mapList(messageService.getConversationWithPage(id, 2L), Message.class);
        Assertions.assertEquals(messageList, messages);
    }
}