package by.senla.socialnetworkingservice.service.impl;

import by.senla.socialnetworkingservice.model.Attachment;
import by.senla.socialnetworkingservice.model.Message;
import by.senla.socialnetworkingservice.repository.AttachmentRepository;
import by.senla.socialnetworkingservice.repository.MessageRepository;
import by.senla.socialnetworkingservice.service.AttachmentService;
import by.senla.socialnetworkingservice.service.util.Converter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.mock.web.MockMultipartFile;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class AttachmentServiceImplTest {

    private AttachmentService attachmentService;
    private final ModelMapper modelMapper = new ModelMapper();
    private final Converter converter = new Converter(modelMapper);

    @Mock
    private AttachmentRepository attachmentRepository;
    @Mock
    private MessageRepository messageRepository;

    private final String mock = "mock";
    private final MockMultipartFile attachment = new MockMultipartFile(mock, mock.getBytes());
    private final Long id = 1L;

    @BeforeEach
    void initAttachmentService() {
        attachmentService = new AttachmentServiceImpl(attachmentRepository, converter, modelMapper, messageRepository);
    }

    @Test
    void addAttachment() {
        //Given
        Message message = new Message();
        message.setSenderId(id);
        message.setId(id);
        BDDMockito.given(messageRepository.findMessageByIdAndSenderId(id, id)).willReturn(java.util.Optional.of(message));

        attachmentService.addAttachment(attachment, id, id);
        Mockito.verify(attachmentRepository).save(Mockito.any(Attachment.class));
    }

    @Test
    void getAttachment() {
        //Given
        Attachment attachment = new Attachment();
        attachment.setId(id);
        BDDMockito.given(attachmentRepository.findById(id)).willReturn(java.util.Optional.of(attachment));

        Assertions.assertEquals(attachment.getId(),attachmentService.getAttachment(id).getId());
    }

    @Test
    void getAllAttachments() {
        //Given
        List<Attachment> attachmentList = new ArrayList<>();
        Attachment attachment1 = new Attachment();
        attachment1.setId(1L);
        Attachment attachment2 = new Attachment();
        attachment2.setId(2L);
        attachmentList.add(attachment1);
        attachmentList.add(attachment2);
        BDDMockito.given(attachmentRepository.findAll()).willReturn(attachmentList);

        List<Attachment> attachments = converter.mapList(attachmentService.getAllAttachments(), Attachment.class);
        Assertions.assertEquals(attachmentList, attachments);
    }

    @Test
    void updateAttachment() {
        //Given
        Message message = new Message();
        message.setSenderId(id);
        message.setId(id);
        BDDMockito.given(messageRepository.findMessageByIdAndSenderId(id, id)).willReturn(java.util.Optional.of(message));
        Attachment attachmentInRepo = new Attachment();
        attachmentInRepo.setId(id);
        BDDMockito.given(attachmentRepository.findById(id)).willReturn(java.util.Optional.of(attachmentInRepo));

        attachmentService.updateAttachment(attachment, id, id, id);
        Mockito.verify(attachmentRepository).save(Mockito.any(Attachment.class));
    }

    @Test
    void deleteAttachment() {
        attachmentService.deleteAttachment(id);
        Mockito.verify(attachmentRepository).deleteById(id);
    }
}