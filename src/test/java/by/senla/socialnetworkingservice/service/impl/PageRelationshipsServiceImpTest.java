package by.senla.socialnetworkingservice.service.impl;

import by.senla.socialnetworkingservice.model.Message;
import by.senla.socialnetworkingservice.model.Page;
import by.senla.socialnetworkingservice.model.PageRelationship;
import by.senla.socialnetworkingservice.model.util.PageRelationshipId;
import by.senla.socialnetworkingservice.repository.PageRelationshipsRepository;
import by.senla.socialnetworkingservice.repository.PageRepository;
import by.senla.socialnetworkingservice.service.PageRelationshipsService;
import by.senla.socialnetworkingservice.service.util.Converter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class PageRelationshipsServiceImpTest {

    private PageRelationshipsService pageRelationshipsService;
    private final ModelMapper modelMapper = new ModelMapper();
    private final Converter converter = new Converter(modelMapper);
    private final Long id1 = 1L;
    private final Long id2 = 2L;
    private final Long id3 = 3L;
    private final Page page1 = new Page();
    private final Page page2 = new Page();
    private final Page page3 = new Page();

    @Mock
    private PageRelationshipsRepository pageRelationshipsRepository;
    @Mock
    private PageRepository pageRepository;

    @BeforeEach
    void initPageRelationshipsService() {
        pageRelationshipsService = new PageRelationshipsServiceImp(converter, pageRepository, pageRelationshipsRepository);
        List<Message> messageList = new ArrayList<>();
        page1.setId(id1);
        page1.setPageMessage(messageList);
        page2.setId(id2);
        page2.setPageMessage(messageList);
        page3.setId(id3);
        page3.setPageMessage(messageList);
    }

    @Test
    void getAllFriends() {
        //Given
        List<Long> ids = List.of(id2, id3);
        List<Page> pages = List.of(page1, page2);
        BDDMockito.given(pageRelationshipsRepository.getFriendForUserById(id1)).willReturn(ids);
        BDDMockito.given(pageRepository.findAllById(ids)).willReturn(pages);

        List<Page> pageList = converter.mapList(pageRelationshipsService.getAllFriends(id1), Page.class);
        Assertions.assertEquals(pages, pageList);
    }

    @Test
    void getAllSubscribers() {
        //Given
        List<Page> pages = List.of(page2);
        List<Long> ids = List.of(id2);
        PageRelationship pageRelationship = new PageRelationship();
        pageRelationship.setRecipientId(id1);
        pageRelationship.setInitiatorId(id2);
        BDDMockito.given(pageRelationshipsRepository.findPageRelationshipsByRecipientIdEquals(id1)).willReturn(List.of(pageRelationship));
        BDDMockito.given(pageRepository.findAllById(ids)).willReturn(pages);

        List<Page> pageList = converter.mapList(pageRelationshipsService.getAllSubscribers(id1), Page.class);
        Assertions.assertEquals(pages, pageList);
    }

    @Test
    void sendRelationshipRequest() {
        //Given
        BDDMockito.given(pageRepository.findPageByOwnerIdAndPersonalTrue(id1)).willReturn(java.util.Optional.of(page1));
        BDDMockito.given(pageRepository.existsById(id2)).willReturn(true);

        pageRelationshipsService.sendRelationshipRequest(id1, id2);
        Mockito.verify(pageRelationshipsRepository).save(Mockito.any(PageRelationship.class));
    }

    @Test
    void unsubscribe() {
        //Given
        BDDMockito.given(pageRepository.findPageByOwnerIdAndPersonalTrue(id1)).willReturn(java.util.Optional.of(page1));

        pageRelationshipsService.unsubscribe(id1, id2);
        Mockito.verify(pageRelationshipsRepository).deleteById(Mockito.any(PageRelationshipId.class));
    }
}