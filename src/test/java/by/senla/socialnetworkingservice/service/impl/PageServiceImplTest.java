package by.senla.socialnetworkingservice.service.impl;

import by.senla.socialnetworkingservice.model.Message;
import by.senla.socialnetworkingservice.model.Page;
import by.senla.socialnetworkingservice.model.util.PageRelationshipId;
import by.senla.socialnetworkingservice.repository.PageRepository;
import by.senla.socialnetworkingservice.service.PageService;
import by.senla.socialnetworkingservice.service.dto.PageDTO;
import by.senla.socialnetworkingservice.service.util.Converter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class PageServiceImplTest {

    private PageService pageService;
    private final ModelMapper modelMapper = new ModelMapper();
    private final Converter converter = new Converter(modelMapper);

    private final Long id = 1L;
    private final PageDTO pageDTO = new PageDTO();
    private final Page page = new Page();
    private final List<Message> messageList = new ArrayList<>();
    private final String pageName = "page";

    @Mock
    private PageRepository pageRepository;

    @BeforeEach
    void initPageService() {
        pageService = new PageServiceImpl(pageRepository, modelMapper, converter);
        page.setId(id);
        page.setPageMessage(messageList);
        page.setPageName(pageName);
        page.setOwnerId(id);
    }

    @Test
    void createPublicPage() {
        pageService.createPublicPage(pageDTO, id);
        Mockito.verify(pageRepository).save(Mockito.any(Page.class));
    }

    @Test
    void findPageById() {
        //Given
        BDDMockito.given(pageRepository.findById(id)).willReturn(java.util.Optional.of(page));

        Page foundPage = modelMapper.map(pageService.findPageById(id), Page.class);
        Assertions.assertEquals(page, foundPage);
    }

    @Test
    void findPageByNameLike() {
        //Given
        BDDMockito.given(pageRepository.findPagesByPageNameLikeIgnoreCase("%" + pageName + "%"))
                .willReturn(Collections.singletonList(page));

        List<Page> foundPages = converter.mapList(pageService.findPageByNameLike(pageName), Page.class);
        Assertions.assertEquals(List.of(page), foundPages);
    }

    @Test
    void updatePage() {
        //Given
        BDDMockito.given(pageRepository.findPageByOwnerIdAndId(id, id)).willReturn(java.util.Optional.of(page));

        pageService.updatePage(pageDTO, id, id);
        Mockito.verify(pageRepository).save(Mockito.any(Page.class));
    }

    @Test
    void deletePage() {
        //Given
        BDDMockito.given(pageRepository.deleteByIdAndOwnerId(id, id)).willReturn(1L);

        pageService.deletePage(id, id);
        Mockito.verify(pageRepository).deleteByIdAndOwnerId(id, id);
    }

    @Test
    void getAllPages() {
        //Given
        BDDMockito.given(pageRepository.findAll()).willReturn(List.of(page));

        List<Page> pages = converter.mapList(pageService.getAllPages(), Page.class);
        Assertions.assertEquals(List.of(page), pages);
    }
}