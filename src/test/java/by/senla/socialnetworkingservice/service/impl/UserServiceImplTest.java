package by.senla.socialnetworkingservice.service.impl;

import by.senla.socialnetworkingservice.model.Page;
import by.senla.socialnetworkingservice.model.User;
import by.senla.socialnetworkingservice.repository.PageRepository;
import by.senla.socialnetworkingservice.repository.UserRepository;
import by.senla.socialnetworkingservice.service.UserService;
import by.senla.socialnetworkingservice.service.dto.UserDTO;
import by.senla.socialnetworkingservice.service.util.Converter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    private UserService userService;
    private final ModelMapper modelMapper = new ModelMapper();
    private final Converter converter = new Converter(modelMapper);
    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    private final UserDTO userDTO = new UserDTO();
    private final Long id = 1L;
    private final String username = "username";
    private final String password = "password";
    private final User user = new User();

    @Mock
    private UserRepository userRepository;
    @Mock
    private PageRepository pageRepository;

    @BeforeEach
    void initUserService() {
        userService = new UserServiceImpl(userRepository, modelMapper, converter, passwordEncoder, pageRepository);
        userDTO.setId(id);
        userDTO.setUsername(username);
        userDTO.setPassword(password);
        user.setId(id);
        user.setUsername(username);
        user.setPassword(password);
    }

    @Test
    void createUser() {
        userService.createUser(userDTO);
        Mockito.verify(pageRepository).save(Mockito.any(Page.class));
        Mockito.verify(userRepository).save(Mockito.any(User.class));
    }

    @Test
    void findByUsername() {
        //Given
        BDDMockito.given(userRepository.findByUsername(username)).willReturn(java.util.Optional.of(user));

        User foundUser = modelMapper.map(userService.findByUsername(username), User.class);
        Assertions.assertEquals(foundUser, user);
    }

    @Test
    void getUser() {
        //Given
        BDDMockito.given(userRepository.findById(id)).willReturn(java.util.Optional.of(user));

        User foundUser = modelMapper.map(userService.getUser(id), User.class);
        Assertions.assertEquals(foundUser, user);
    }

    @Test
    void updateUser() {
        //Given
        BDDMockito.given(userRepository.findById(id)).willReturn(java.util.Optional.of(user));

        userService.updateUser(id, userDTO);
        Mockito.verify(userRepository).save(Mockito.any(User.class));
    }

    @Test
    void deleteUser() {
        userService.deleteUser(id);
        Mockito.verify(userRepository).deleteById(id);
    }

    @Test
    void getAllUsers() {
        //Given
        List<User> users = List.of(user);
        BDDMockito.given(userRepository.findAll()).willReturn(users);

        List<User> foundUsers = converter.mapList(userService.getAllUsers(), User.class);
        Assertions.assertEquals(foundUsers, users);
    }

    @Test
    void changePassword() {
        //Given
        BDDMockito.given(userRepository.findById(id)).willReturn(java.util.Optional.of(user));

        userService.changePassword(id, password);
        Mockito.verify(userRepository).save(Mockito.any(User.class));
    }
}